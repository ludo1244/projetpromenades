-- Utilisons cette base de données
USE projetPromenades;


-- Création de la table seance
CREATE TABLE promenades (
    id INT PRIMARY KEY AUTO_INCREMENT,
    titre VARCHAR(255) NOT NULL,
    auteur VARCHAR(255) NOT NULL,
    pays VARCHAR(255) NOT NULL,
    ville VARCHAR(255) NOT NULL,
    codePostal VARCHAR(255) NOT NULL,
    depart VARCHAR(255) NOT NULL,
    arrivee VARCHAR(255) NOT NULL,
    description TEXT,
    photo VARCHAR(255)
);

