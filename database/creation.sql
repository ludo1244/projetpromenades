-- Script de création de la base de données
-- création de la base de données
CREATE DATABASE projetPromenades;

-- Utilisons cette nouvelle base de données
USE projetPromenades;

-- Création d'un utilisateur admin 
-- et on lui donne tous les droits sur la base de données
-- et sur toutes les tables de la base
CREATE USER 'adminPromenades'@'%' IDENTIFIED BY 'mmPeTE942';
GRANT ALL PRIVILEGES ON projetPromenades.* TO 'adminPromenades'@'%';