<?php
class Promenade{
    private $id;
    private $titre;
    private $auteur;
    private $pays;
    private $ville;
    private $codePostal;
    private $depart;
    private $arrivee;
    private $description;
    private $photo;

    public function __construct(){}

    public static function createPromenade($titre,$auteur,$pays,$ville, $codePostal, $depart, $arrivee, $description, $photo){
        $promenade = new self();
        $promenade->setTitre($titre);
        $promenade->setAuteur($auteur);
        $promenade->setPays($pays);
        $promenade->setVille($ville);
        $promenade->setCodePostal($codePostal);
        $promenade->setDepart($depart);
        $promenade->setArrivee($arrivee);
        $promenade->setDescription($description);
        $promenade->setPhoto($photo);
        return $promenade;
    }
    //getters
    public function getId(){return $this->id;}
    public function getTitre(){return $this->titre;}
    public function getAuteur(){return $this->auteur;}
    public function getPays(){return $this->pays;}
    public function getVille(){return $this->ville;}
    public function getCodePostal(){return $this->codePostal;}
    public function getDepart(){return $this->depart;}
    public function getArrivee(){return $this->arrivee;}
    public function getDescription(){return $this->description;}
    public function getPhoto(){return $this->photo;}

    //setters
    public function setId($id){$this->id = $id;}
    public function setTitre($titre){$this->titre = $titre;}
    public function setAuteur($auteur){$this->auteur = $auteur;}
    public function setPays($pays){$this->pays = $pays;}
    public function setVille($ville){$this->ville = $ville;}
    public function setCodePostal($codePostal){$this->codePostal = $codePostal;}
    public function setDepart($depart){$this->depart = $depart;}
    public function setArrivee($arrivee){$this->arrivee = $arrivee;}
    public function setDescription($description){$this->description = $description;}
    public function setPhoto($photo){$this->photo = $photo;}
}