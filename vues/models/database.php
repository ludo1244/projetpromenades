<?php
//class 
include_once(dirname(__FILE__)."/promenade.php");


class Database{

    //constante de connexion
    const DB_HOST = "mariadb";
    const DB_PORT = "3306";
    const DB_DATABASE_NAME = "projetPromenades";
    const DB_USER = "adminPromenades";
    const DB_PASSWORD = "mmPeTE942";

    private $connexion;

    //initialiser la connexion
    public function __construct(){
        $this->connexion = new PDO(
            "mysql:host=" . self::DB_HOST . ";dbname=" . self::DB_DATABASE_NAME . ";",
            self::DB_USER,
            self::DB_PASSWORD
        );
    }
//INSERT INTO `promenades` (`id`, `titre`, `auteur`, `pays`, `ville`, `codePostal`, `depart`, `arrivee`, `description`, `photo`) VALUES (NULL, 'tour de la seymaz', 'ludo', 'suisse', 'geneve', '1244', 'Choulex', 'Choulex', 'tour dans Choulex', NULL);

    //fonctions
    // fonction pour inserer une promenade dans la BD
    public function createPromenade(Promenade $promenade){
        // Etape 1 : préparation de la requête
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO `promenades` (`titre`, `auteur`, `pays`, `ville`, `codePostal`, `depart`, `arrivee`, `description`, `photo`) VALUES (:titre, :auteur, :pays, :ville, :codePostal, :depart, :arrivee, :description, :photo);"
        );
        // Etape 2 : exécution de la requête (avec paramètres)
        $pdoStatement->execute([
            "titre"    => $promenade->getTitre(),
            "auteur"       => $promenade->getAuteur(),
            "pays" => $promenade->getPays(),
            "ville"     => $promenade->getVille(),
            "codePostal"     => $promenade->getCodePostal(),
            "depart"     => $promenade->getDepart(),
            "arrivee"     => $promenade->getArrivee(),
            "description"     => $promenade->getDescription(),
            "photo"     => $promenade->getPhoto(),
        ]);
        if($pdoStatement->errorCode()== 0){
            return $this->connexion->lastInsertId();
        }else{
            return false;
        }
        
    }

    //Chercher un séance par id
    public function getPromenadeById($id){
        // Etape 1 : préparation de la requête
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM promenades WHERE id = :id"
        );
        // Etape 2 : exécution de la requête (avec paramètres)
        $pdoStatement->execute([
            "id"    => $id
        ]);
        $promenade = $pdoStatement->fetchObject("Promenade");
        return $promenade;

    }
    //Chercher un séance par id
    public function getAllPromenade(){
        // Etape 1 : préparation de la requête
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM promenades"
        );
        // Etape 2 : exécution de la requête (avec paramètres)
        $pdoStatement->execute([
        ]);
        $promenade = $pdoStatement->fetchAll(PDO::FETCH_CLASS,"Promenade");
        if($pdoStatement->errorCode()== 0){
            return $promenade;
        }else{
            return false;
        }

    }
}