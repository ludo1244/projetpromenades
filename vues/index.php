<?php
    include("modules/partie1.php");

    require_once(__DIR__."/models/database.php");
    $database = new Database();

    $promenades = $database->getAllPromenade();
?>
<div class="container card text-center">
    <h1 class="card-header">Bienvenue promeneur</h1>
    <div class="card-body">
        <a href="ajout-promenade.php">crée une nouvelle promenade</a>
        <div class="row font-weight-bold">
            <div class="col-3">
                Titre
            </div>
            <div class="col-3">
                Auteur
            </div>
            <div class="col-3">
                Pays
            </div>
            <div class="col-3">
                Ville
            </div>
        </div>
            <?php foreach($promenades as $promenade){?>
                <a href="promenade.php?id=<?php echo $promenade->getId() ?>">
                    <div class="row">
                        <div class="col-3">
                            <?php echo $promenade->getTitre()?>
                        </div>
                        <div class="col-3">
                            <?php echo $promenade->getAuteur()?>
                        </div>
                        <div class="col-3">
                            <?php echo $promenade->getPays()?>
                        </div>
                        <div class="col-3">
                            <?php echo $promenade->getVille()?>
                        </div>
                    </div>
                </a>              
            <?php } ?>
    </div>
</div>

<?php
    include("modules/partie3.php");    
?>