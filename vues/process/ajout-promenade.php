<?php
session_start();

require_once(__DIR__."/../models/database.php");
$database = new Database();

//récupération des données du formulaire
$titre = isset($_POST["titre"]) ? $_POST["titre"] : "";
$auteur = isset($_POST["auteur"]) ? $_POST["auteur"] : "";
$pays = isset($_POST["pays"]) ? $_POST["pays"] : "";
$ville = isset($_POST["ville"]) ? $_POST["ville"] : "";
$codePostal = isset($_POST["codePostal"]) ? $_POST["codePostal"] : "";
$depart = isset($_POST["depart"]) ? $_POST["depart"] : "";
$arrivee = isset($_POST["arrivee"]) ? $_POST["arrivee"] : "";
$description = isset($_POST["description"]) ? $_POST["description"] : "";


$promenade = Promenade::createPromenade($titre,$auteur,$pays,$ville,$codePostal,$depart,$arrivee,$description,"");

$error = null;
$succes = null;

$id = $database->createPromenade($promenade);

if ($id){
    $succes = "La promenade a été créee avec succes";
}else{
    $error = "La création de la promenade a rencontré une erreur";
}


if ($error == null){
    $_SESSION["info"] = $succes;
    header('Location: ../index.php');
}else{
    $_SESSION["error"] = $error;
    header("location: ../ajout-promenade.php");
}