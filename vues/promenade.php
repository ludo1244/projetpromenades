<?php
    include("modules/partie1.php");

    require_once(__DIR__."/models/database.php");
    $database = new Database();

    //recupere l'id dans l'url ou prend 0 par défaut
    $idPromenade = isset($_GET["id"]) ? $_GET["id"] : 0;

    if (!$idPromenade){
        $_SESSION["error"] = "Promenade pas trouvée";
        echo '<script type="text/javascript">';
        echo 'window.location.href="index.php"';
        echo '</script>';
    }

    $promenade = $database->getPromenadeById($idPromenade);
?>
<div class="container card text-center">
    <h1 class="card-header"><?php echo $promenade->getTitre(); ?></h1>
    <div class="card-body">
        <!-- auteur -->
        <div class="row mb-4">
            <div class="col-4 font-weight-bold">
                Auteur :
            </div>
            <div class="col-8">
                <?php echo $promenade->getAuteur(); ?>
            </div>
        </div>
        <!-- pays -->
        <div class="row mb-4">
            <div class="col-4 font-weight-bold">
                Pays :
            </div>
            <div class="col-8">
                <?php echo $promenade->getPays(); ?>
            </div>
        </div>
        <!-- ville -->
        <div class="row mb-4">
            <div class="col-4 font-weight-bold">
                Ville :
            </div>
            <div class="col-8">
                <?php echo $promenade->getVille(); ?>
            </div>
        </div>
        <!-- code postal -->
        <div class="row mb-4">
            <div class="col-4 font-weight-bold">
                Code postal :
            </div>
            <div class="col-8">
                <?php echo $promenade->getCodePostal(); ?>
            </div>
        </div>
        <!-- depart -->
        <div class="row mb-4">
            <div class="col-4 font-weight-bold">
                Lieu de depart :
            </div>
            <div class="col-8">
                <?php echo $promenade->getDepart(); ?>
            </div>
        </div>
        <!-- arrivee -->
        <div class="row mb-4">
            <div class="col-4 font-weight-bold">
                Lieu d'arrivée :
            </div>
            <div class="col-8">
                <?php echo $promenade->getArrivee(); ?>
            </div>
        </div>
        <!-- description -->
        <div class="row mb-4">
            <div class="col font-weight-bold">
                Description :
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-12">
                <?php echo $promenade->getDescription(); ?>
            </div>
        </div>
    </div>
</div>

<?php
    include("modules/partie3.php");    
?>