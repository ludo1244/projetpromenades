<?php
    include("modules/partie1.php");

    
?>
<div class="container card text-center">
    <h1 class="card-header">Creation promenade</h1>
    <div class="card-body">
        <form class="text-left text-md-right" action="process/ajout-promenade.php" method="POST">
            <div class="row form-group">
                <label for="titre" class="col-sm-12 col-md-4 col-form-label">Titre</label>
                <div class="col-sm-12 col-md-8">
                    <input type="text" class="form-control" id="titre" name="titre" placeholder="Titre" value="" required>
                </div>
            </div>
            <div class="row form-group">
                <label for="auteur" class="col-sm-12 col-md-4 col-form-label">Auteur</label>
                <div class="col-sm-12 col-md-8">
                    <input type="text" class="form-control" id="auteur" name="auteur" placeholder="Auteur" value="" required>
                </div>
            </div>
            <div class="row form-group">
                <label for="pays" class="col-sm-12 col-md-4 col-form-label">Pays</label>
                <div class="col-sm-12 col-md-8">
                    <input type="text" class="form-control" id="pays" name="pays" placeholder="Pays" value="" required>
                </div>
            </div>
            <div class="row form-group">
                <label for="ville" class="col-sm-12 col-md-4 col-form-label">Ville</label>
                <div class="col-sm-12 col-md-8">
                    <input type="text" class="form-control" id="ville" name="ville" placeholder="Ville" value="" required>
                </div>
            </div>
            <div class="row form-group">
                <label for="codePostal" class="col-sm-12 col-md-4 col-form-label">Code postal</label>
                <div class="col-sm-12 col-md-8">
                    <input type="text" class="form-control" id="codePostal" name="codePostal" placeholder="Code postal" value="" required>
                </div>
            </div>
            <div class="row form-group">
                <label for="depart" class="col-sm-12 col-md-4 col-form-label">lieu de depart</label>
                <div class="col-sm-12 col-md-8">
                    <input type="text" class="form-control" id="depart" name="depart" placeholder="lieu de depart" value="" required>
                </div>
            </div>
            <div class="row form-group">
                <label for="arrivee" class="col-sm-12 col-md-4 col-form-label">lieu d'arrivée</label>
                <div class="col-sm-12 col-md-8">
                    <input type="text" class="form-control" id="arrivee" name="arrivee" placeholder="lieu de depart" value="" required>
                </div>
            </div>
            <div class="row form-group">
                <label for="description" class="col-sm-12 col-md-4 col-form-label">Description</label>
                <div class="col-sm-12 col-md-8">
                    <textarea class="form-control" id="description" name="description"></textarea>
                </div>
            </div>
            <!--
            <div class="row form-group">
                <label for="description" class="col-sm-12 col-md-4 col-form-label">Description</label>
                <div class="col-sm-12 col-md-8">
                    <textarea class="form-control" id="description" name="description"></textarea>
                </div>
            </div>
            -->
            <div class="form-group text-center">
                <button class="btn btn-dark" type="submit">Cree promenade</button>
            </div>
        </form>
    </div>
</div>

<?php
    include("modules/partie3.php");    
?>