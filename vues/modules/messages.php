<?php if (isset($_SESSION["error"])){
    // si on a une erreur dans la session, on l'affiche
?>
<div class="alert alert-danger" role="alert">
    <strong><?php echo $_SESSION["error"];?></strong>
</div>
<?php
    $_SESSION["error"] = NULL;
}
?>

<?php if (isset($_SESSION["info"])){
    //si on a une info dans la session, on l'affiche
?>
<div class="alert alert-danger" role="alert">
    <strong><?php echo $_SESSION["info"];?></strong>
</div>
<?php
    $_SESSION["info"] = NULL;
}
